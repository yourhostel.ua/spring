package app;

import org.springframework.stereotype.Service;

import java.util.List;

//@Component for injection deprecated
@Service//for injection
public class BookServiceMocked implements BookService {
    private final List<Book> data = List.of(
            new Book("java", "Ernest", 2012),
            new Book("scala", "Jack", 2015),
            new Book("haskell", "Jin", 2020)
    );
    @Override
    public List<Book> getAll(){
        return data;
    }
}
