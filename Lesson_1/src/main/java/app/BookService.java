package app;

import java.util.List;

public interface BookService {
    List<Book> getAll();
}
