package app;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController //to JSON
//@Controller
public class BookController {
    //@Autowired deprecated works with @Сomponent on a service for injection
    private final BookService service;

    @GetMapping("books")
    //@ResponseBody //to JSON
    public List<Book> allBooks() {
        return service.getAll();
    }

    //http://localhost:8080/book?id=1
    @GetMapping("book")
    public Book bookById(@RequestParam("id") Integer n) {
        return service.getAll().get(n - 1);
    }

    //http://localhost:8080/book_optional?id=1 or //http://localhost:8080/book_optional
    @GetMapping("book_optional")
    public Book bookById1(@RequestParam("id") Optional<Integer> n) {
        return service.getAll().get(n.orElse(3) - 1);
    }

    //http://localhost:8080/book/1
    @GetMapping("book/{id}")
    public Book bookById0(@PathVariable("id") Integer n) {
        return service.getAll().get(n - 1);
    }
}
