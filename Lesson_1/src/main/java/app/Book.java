package app;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Book{
    String title;
    String author;
    Integer year;
}
