package ua.entities;

import org.springframework.stereotype.Component;

@Component
public class Dog {
    private String name = "hot-dog";

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
