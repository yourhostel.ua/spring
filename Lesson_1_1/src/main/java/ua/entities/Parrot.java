package ua.entities;

import org.springframework.stereotype.Component;

@Component("parrot-kesha")
public class Parrot {
    String name = "kesha";

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
