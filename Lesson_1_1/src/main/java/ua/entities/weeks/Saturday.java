package ua.entities.weeks;

public class Saturday implements WeekDay{
    @Override
    public String getWeekDayName() {
        return "Saturday";
    }
}
