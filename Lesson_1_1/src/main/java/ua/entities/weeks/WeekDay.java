package ua.entities.weeks;

public interface WeekDay {
    String getWeekDayName();
}
