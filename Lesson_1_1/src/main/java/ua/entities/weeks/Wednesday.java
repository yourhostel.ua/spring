package ua.entities.weeks;

public class Wednesday implements WeekDay{
    @Override
    public String getWeekDayName() {
        return "Wednesday";
    }
}
