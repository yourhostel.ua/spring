package ua.entities.weeks;

public class Tuesday implements WeekDay{
    @Override
    public String getWeekDayName() {
        return "Tuesday";
    }
}
