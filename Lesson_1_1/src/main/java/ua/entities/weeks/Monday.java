package ua.entities.weeks;

public class Monday implements WeekDay{

    @Override
    public String getWeekDayName() {
        return "Monday";
    }
}
