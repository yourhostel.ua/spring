package ua.entities.weeks;

public class Thursday implements WeekDay{
    @Override
    public String getWeekDayName() {
        return "Thursday";
    }
}
