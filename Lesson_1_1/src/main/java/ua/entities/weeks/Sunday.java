package ua.entities.weeks;

public class Sunday implements WeekDay{
    @Override
    public String getWeekDayName() {
        return "Sunday";
    }
}
